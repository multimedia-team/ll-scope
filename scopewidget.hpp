/****************************************************************************
    
    scopewidget.hpp - an Oscilloscope widget for gtkmm
    
    Copyright (C) 2005  Lars Luthman <larsl@users.sourceforge.net>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 01222-1307  USA

****************************************************************************/

#ifndef SCOPEWIDGET_HPP
#define SCOPEWIDGET_HPP

#include <gtkmm.h>
#include <ladspa.h>

#include "ll-scope.h"


using namespace std;
using namespace Gtk;
using namespace Gdk;
using namespace Glib;
using namespace sigc;


class ScopeWidget : public DrawingArea {
public:

  ScopeWidget(shared_data_t* shared_data);
  ~ScopeWidget();
  
  void set_trigger_level(LADSPA_Data trigger_level);
  void set_trigger_direction(bool up);
  void set_y1_scale(double scale);
  void set_y2_scale(double scale);
  void set_y1_offset(double offset);
  void set_y2_offset(double offset);
  void set_x_offset(double offset);
  void set_frozen(bool frozen);
  void set_additive(bool additive);
  void set_time_scale(double ms);
  
  void on_realize();
  bool on_expose_event(GdkEventExpose* event);
  bool on_button_press_event(GdkEventButton* event);
  void clear();
  
  // signals
  signal<void> middle_button_pressed;
  
private:
  
  bool read_data();
  void draw_data();
  
  RefPtr<Colormap> m_colormap;
  Color m_bg_color, m_fg_color, m_fg_color2, m_trig_color,
    m_grid_color1, m_grid_color2;
  RefPtr<Gdk::Window> m_win;
  RefPtr<GC> m_gc;
  RefPtr<Pixmap> m_background;
  
  shared_data_t* m_shared_data;
  ringbuf_t* m_buffer1;
  ringbuf_t* m_buffer2;
  LADSPA_Data* m_channel1;
  LADSPA_Data* m_channel2;
  unsigned int m_frames_read;
  
  int m_timeout;
  LADSPA_Data m_trigger_level;
  bool m_trigger_up;
  double m_y1_scale;
  double m_y2_scale;
  double m_y1_offset;
  double m_y2_offset;
  double m_x_offset;
  bool m_frozen;
  bool m_additive;
  LADSPA_Data  m_trigger_phase;
  
  unsigned int m_window_size;
  unsigned long m_sample_rate;
  
  enum {
    WaitingForTrigger,
    ReadingData
  } m_scope_state;
  int m_samples_read;
};


#endif
