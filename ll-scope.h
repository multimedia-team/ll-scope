/****************************************************************************
    
    ll-scope.h - The plugin part of the DSSI Oscilloscope plugin
    
    Copyright (C) 2005  Lars Luthman <larsl@users.sourceforge.net>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 01222-1307  USA

****************************************************************************/

#ifndef LL_SCOPE_H
#define LL_SCOPE_H

#include <ladspa.h>

#include "ringbuffer.h"


/** @file
    This file contains datatypes that are used by the scope plugin.
*/


#if defined(__cplusplus)
extern "C" {
#endif
  
  /** The BUFFER_SIZE is the number of sample frames that the ringbuffers can
      hold. */
#define BUFFER_SIZE 128000

  /** This type contains the data that is stored in the memory segment that is
      shared between the plugin and the UI. It has two ringbuffers, one for each
      channel of audio data. */
  typedef struct {
    unsigned long sample_rate;
    union {
      ringbuf_t rb;
      char data[sizeof(ringbuf_t) - 1 + BUFFER_SIZE * sizeof(LADSPA_Data)];
    } channel1;
    union {
      ringbuf_t rb;
      char data[sizeof(ringbuf_t) - 1 + BUFFER_SIZE * sizeof(LADSPA_Data)];
    } channel2;
  } shared_data_t;
  
  
#if defined(__cplusplus)
}
#endif


#endif

/* For the Doxygen main page */
/** @mainpage Oscilloscope plugin source code documentation
    This source code documentation is generated in order to help people
    (including me) understand how the Oscilloscope plugin works, or is 
    supposed to work. But it's not meant to be a complete reference, so 
    many functions and types will be undocumented.
    
    Send questions and comments to larsl@users.sourceforge.net. 
*/
